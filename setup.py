# -*- coding: utf-8 -*-
from setuptools import setup, find_packages


requires = [
    # 'cerbere>=2.0.0',
    'cfgrib'
]

setup(
    name='cerberecontrib-grib',
    version='2.0',
    url='',
    download_url='',
    license='GPLv3',
    author='Jeff Piolle',
    author_email='jfpiolle@ifremer.fr',
    description='Cerbere extension for GRIB format',
    long_description="This package contains the Cerbere extension for GRIB format.",
    zip_safe=False,
    classifiers=[
        'Development Status :: 4 - Beta',
        'Environment :: Console',
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GPLv3 License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Documentation',
        'Topic :: Utilities',
    ],
    entry_points={
        'cerbere.plugins': [
            'GRIBDataset = cerberecontrib_grib.dataset.gribdataset:GRIBDataset',
            'ECMWFForecastGRIBDataset = cerberecontrib_grib.dataset.ecmwfforecastgribdataset:ECMWFForecastGRIBDataset',
        ]
    },
    platforms='any',
    packages=find_packages(),
    include_package_data=True,
    package_data={},
    install_requires=requires,
    namespace_packages=[],
)
