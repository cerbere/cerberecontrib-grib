===========================================
Cerbere extensions for GRIB format datasets
===========================================

Installation
============

Preferably install in a conda environment!

Requires:
  * ``cfgrib`` (including ``eccodes`` dependency)
  * ``cerbere>=2.0.0``


Example
=======

.. code-block:: python

  import cerbere
  dst = cerbere.open_dataset('ensemble_mean_era5_adaptor.mars.internal-1585319725.9502103-25518-23-67971843-d4b3-4ad8-ab2b-60288f797ec1.grib', 'GRIBDataset')

  # print description and metadata
  print(dst)



