"""

Test class for cerbere GRIB dataset

:copyright: Copyright 2020 Ifremer / Cersat.
:license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
"""
import unittest

from tests.checker import Checker


class GRIBERA5DatasetChecker(Checker, unittest.TestCase):
    """Test class for GRIBfiles"""

    def __init__(self, methodName="runTest"):
        super(GRIBERA5DatasetChecker, self).__init__(methodName)

    @classmethod
    def dataset(cls):
        """Return the mapper class name"""
        return 'GRIBDataset'

    @classmethod
    def feature(cls):
        """Return the related feature class name"""
        return 'CylindricalGridTimeSeries'

    @classmethod
    def test_file(cls):
        """Return the name of the test file for this test"""
        return "era5-levels-members.grib"

    @classmethod
    def download_url(cls):
        """Return the URL of the data test repository where to get the test
        files
        """
        return "ftp://ftp.ifremer.fr/ifremer/cersat/projects/cerbere/test_data/"
