# -*- coding: utf-8 -*-
"""
Dataset class for GRIB format
"""
from cerbere.dataset.dataset import Dataset


class GRIBDataset(Dataset):
    """Dataset class to read GRIB files"""

    def _open_dataset(self, **kwargs):
        """
        Open a XArrayDataset compatible file (netCDF, ZArr,...)
        """
        return super(GRIBDataset, self)._open_dataset(engine='cfgrib', **kwargs)
