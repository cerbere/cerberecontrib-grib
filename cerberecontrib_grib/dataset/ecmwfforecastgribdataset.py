# -*- coding: utf-8 -*-
"""
Dataset class for ECMWF Forecast and analyses in GRIB format
"""
from .gribdataset import GRIBDataset


class ECMWFForecastGRIBDataset(GRIBDataset):
    """Dataset class to read ECMWF Forecast and analyses GRIB files"""

    def _transform(self):
        super(ECMWFForecastGRIBDataset, self)._transform()

        # add dimensions to dimensionless coordinates
        for k, v in self._std_dataset.coords.items():
            if len(v.dims) == 0:
                self._std_dataset.coords[k] = v.expand_dims([k])
